exports = (typeof window === 'undefined') ? global : window;

exports.recursionAnswers = {
  permute: function(arr) {
      // take for example an array like:
      // var arr = [1 , 5 ,7];
      // executing the permute function
      // >> var result = permute(arr);
      // should return the following result:
      // [
      //    [1, 5, 7]
      //    [1, 7, 5]
      //    [5, 1, 7]
      //    [5, 7, 1]
      //    [7, 1, 5]
      //    [7, 5, 1]
      // ]
  },

  fibonacci: function(n) {
      // executing the function like:
      // fibonacci(2)
      // it should return
      // 1
  },

  validParentheses: function(n) {
      // executing the function like:
      // validParentheses(3)
      // it should return an array
      // ['((()))', '(()())', '(())()', '()(())', '()()()']
      // all the valid combinations of three pairs of parentheses.
  }
};
